# postgres-debezium-s3

A poc to stream postgres data to s3.

## Install

```sh
poetry install
```

## Setup

Download kafka-connect-s3 from https://www.confluent.io/hub/confluentinc/kafka-connect-s3.
Unzip and place the folder at the root of this project.
Different s3 sink connector are available, choose one and edit `init.sh`.
Edit `s3.proxy.url` in `register-s3-sink-connector-*.json` with your ip address.

## Run

```sh
# Start the topology as defined in https://debezium.io/docs/tutorial/
docker-compose up -d

./init.sh

# Modify records in the database via Postgres client
docker-compose exec postgres env PGOPTIONS="--search_path=inventory" bash -c 'psql -U $POSTGRES_USER postgres'

# Consume messages from a Debezium topic
docker-compose exec kafka /kafka/bin/kafka-console-consumer.sh \
    --bootstrap-server kafka:9092 \
    --from-beginning \
    --property print.key=true \
    --topic dbserver1.inventory.products

# Shut down the cluster
docker-compose down
```

### S3 output

With `register-s3-sink-connector-confluent.json`:

```sh
$ awslocal s3 ls s3://dbzs3/topics/dbserver1.inventory.products/partition=0/
2021-01-24 18:59:02       3355 dbserver1.inventory.products+0+0000000000.json

$ awslocal s3 cp --quiet s3://dbzs3/topics/dbserver1.inventory.products/partition=0/dbserver1.inventory.products+0+0000000000.json /dev/stdout
{"before":null,"after":{"id":101,"name":"scooter","description":"Small 2-wheel scooter","weight":3.14},"source":{"version":"1.4.0.Final","connector":"postgresql","name":"dbserver1","ts_ms":1611511110990,"snapshot":"true","db":"postgres","schema":"inventory","table":"products","txId":596,"lsn":33816576,"xmin":null},"op":"r","ts_ms":1611511110991,"transaction":null}
...
```

With `register-s3-sink-connector-dataroc.json` (does not handle delete):

```sh
awslocal s3 cp --quiet s3://dbzs3/topics/dbserver1.inventory.products/server_date=2021-01-24/hour=20/minute=00/dbserver1.inventory.products+0+0000000009.json /dev/stdout
{"schema":{"name":"dbserver1.inventory.products.Envelope","optional":false,"type":"struct","fields":[{"field":"before","name":"dbserver1.inventory.products.Value","optional":true,"type":"struct","fields":[{"field":"id","optional":false,"type":"int32"},{"field":"name","optional":false,"type":"string"},{"field":"description","optional":true,"type":"string"},{"field":"weight","optional":true,"type":"double"}]},{"field":"after","name":"dbserver1.inventory.products.Value","optional":true,"type":"struct","fields":[{"field":"id","optional":false,"type":"int32"},{"field":"name","optional":false,"type":"string"},{"field":"description","optional":true,"type":"string"},{"field":"weight","optional":true,"type":"double"}]},{"field":"source","name":"io.debezium.connector.postgresql.Source","optional":false,"type":"struct","fields":[{"field":"version","optional":false,"type":"string"},{"field":"connector","optional":false,"type":"string"},{"field":"name","optional":false,"type":"string"},{"field":"ts_ms","optional":false,"type":"int64"},{"default":"false","field":"snapshot","name":"io.debezium.data.Enum","optional":true,"type":"string","version":1,"parameters":{"allowed":"true,last,false"}},{"field":"db","optional":false,"type":"string"},{"field":"schema","optional":false,"type":"string"},{"field":"table","optional":false,"type":"string"},{"field":"txId","optional":true,"type":"int64"},{"field":"lsn","optional":true,"type":"int64"},{"field":"xmin","optional":true,"type":"int64"}]},{"field":"op","optional":false,"type":"string"},{"field":"ts_ms","optional":true,"type":"int64"},{"field":"transaction","optional":true,"type":"struct","fields":[{"field":"id","optional":false,"type":"string"},{"field":"total_order","optional":false,"type":"int64"},{"field":"data_collection_order","optional":false,"type":"int64"}]}]},"payload":{"op":"c","before":null,"after":{"name":"test","description":null,"weight":null,"id":110},"source":{"schema":"inventory","xmin":null,"connector":"postgresql","lsn":33837696,"name":"dbserver1","txId":597,"version":"1.4.0.Final","ts_ms":1611518714305,"snapshot":"false","db":"postgres","table":"products"},"ts_ms":1611518714523,"transaction":null}}
```

## Ressources

- https://github.com/debezium/debezium-examples/tree/master/tutorial#using-postgres
- https://docs.confluent.io/kafka-connect-s3-sink/current/index.html
- https://github.com/localstack/localstack#using-docker-compose

- https://medium.com/dataorc/your-first-data-platform-a51ffe3483a8
- https://fr.slideshare.net/EdwardBloom/devfest-nantes-2018-crer-un-data-pipeline-en-20-minutes-avec-kafka-connect

## Questions

- Handle multiple topics at once or register for each topic?
  - Possible with regex, one bucket = one connector
- What does Athena eats?
- Handle update and delete?
- Schema changes?

## Notes

- [event flattening](https://debezium.io/documentation/reference/1.4/configuration/event-flattening.html)
